/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import { createStackNavigator } from 'react-navigation';

import HomeScreen from './rn-components/HomeScreen';
import TodoList from './mobx/listStore';

const rootStack = createStackNavigator(
  {
    Home: HomeScreen,
    TodoList: TodoListScreen
  },
  {
    initialRouteName: ' Home',
    initialRouteParams: {
      store: TodoList
    }
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack/>;
  }
}


