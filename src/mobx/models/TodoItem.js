
type TodoItem = {
  name: string,
  items: Array<TodoItem>
};

export default TodoItem;
